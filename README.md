Steps to generate a visualization:


1. Run the new xml-parser.py which can be found in this github as you have previously been doing. This would now generate a file called **writeFile.json** in the **data** folder.

2. Instructions to the dev team that previously compiled the code:

        1. Download the last version of the d3js code from (d3js code)[https://observablehq.com/@ankuj/untitled]
        
        2. Replace the previous one you complied with the code from Step 2.1
        
        3. Replace the URL in line `data = d3.json("https://api.myjson.com/bins/g29ru")` of the code from Step 2.1 with the path to the writeFile.json
        produced in step 1.
        
        4. Run the d3js code as you did before. It should work.
        
        5. In case Step 2.4 does not work, feed the writeFile.json to the site (myJson API)[http://myjson.com/] to produce a URL version of the json file.
        Copy that URL into the line `data = d3.json("https://api.myjson.com/bins/g29ru")` of the code in Step 2.1 and execute. It should work.
        
        
        
3. Voila, contact me in case of issues. Bonne reception.