#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# modules
import sys, random
import xml.etree.ElementTree
import json
import matplotlib.pyplot as plt
from pathlib import Path

# number of colors that appear in the visualization can be set here
number_of_colors = 100
# create array of all used colors
color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)]) for i in range(number_of_colors)]

def main():
    #produce json file inside the data folder
    with open(str(Path().absolute()) + '/data/writeFile.json', 'w') as outfile:
        """
        Run the script : python xml-parser.py CMI359_18-12-17-annote.xml
        """
        # input file in command line
        xml_file = sys.argv[1]
    
        # parse input file
        tree = xml.etree.ElementTree.parse(xml_file)
        root = tree.getroot()
    
        print("Study's id : " + str(root.attrib['idEtude']))
        print("Number of groups : " + str(root.attrib['groupesTemoins']))
    
        nb_questions = 0
    
        #dictionary for json representation
        rootDictionary, conceptDictionary = {}, {}
        #define the root of the tree
        rootDictionary['name'] = 'root'
        groupDictionaryList = []
        rootDictionary['children'] = groupDictionaryList
        rootDictionary['color'] = "#E8E8E8"
        # parsing
        for question in root:
            nb_questions += 1
            count=0
            conceptNameList = []
            #dictionary to keep track of the concepts and their colors across groups
            conceptColorDictionary = {}
            for groupe in question:
                innerList = []
                groupDictionary = {}
                groupDictionary["name"] = str(groupe.attrib['nomGroupe'])
                #set colors for group here
                groupDictionary['color'] = '#E8E8E8'
                for reponse in groupe:
                    for concept in reponse:
                        conceptInInnerList = False
                        conceptDictionary, ideaDictionary = {},{}  
                        ideaSet = set()                      
                        #pull out ideas per concept
                        ideaDictionary['name'] = concept.text
                        ideaDictionary['value'] = 1
                        ideaDictionary['color'] = "#FFFFFF"
                        ideaSet.add(concept.text)
                        conceptName = str(concept.attrib['conceptGlobal'])
                        if innerList==[]:
                            #new concept that must be added to a certain dictionary
                            conceptDictionary["name"] = conceptName
                            conceptDictionary["color"] = color[count]
                            conceptColorDictionary[conceptName] = conceptDictionary["color"]
                            count+=1
                            conceptDictionary["value"] = 1
                            innerList.append(conceptDictionary)
                            conceptNameList.append(conceptName)
                            conceptDictionary['ideaConcat'] = concept.text
                        if conceptName in conceptNameList:
                            for k in innerList:
                                if conceptName == k["name"]:
                                    k["value"] += 1
                                    conceptInInnerList = True;
                                    if(concept.text not in k['ideaConcat']):
                                        k['ideaConcat'] = k['ideaConcat'] + ' <br> ' + concept.text
                                else: continue
                            # if the concept name has been spotted before in some other group,
                            # but the dictionary for the current group does not have that concept
                            if not conceptInInnerList:
                                conceptDictionary["name"] = conceptName
                                conceptDictionary["value"] = 1
                                conceptDictionary["color"] = conceptColorDictionary[conceptName]
                                count+=1
                                innerList.append(conceptDictionary)
                                conceptDictionary['ideaConcat'] =  concept.text
                        else:
                        #new concept that must be added to a certain dictionary
                            conceptDictionary["name"] = conceptName
                            conceptDictionary["value"] = 1
                            conceptDictionary["color"] = color[count]
                            conceptColorDictionary[conceptName] = conceptDictionary["color"]
                            conceptDictionary["ideaConcat"] = concept.text                            
                            count+=1
                            innerList.append(conceptDictionary)
                            conceptNameList.append(conceptName)
                        conceptInInnerList = False
                groupDictionary["children"] = innerList
                groupDictionaryList.append(groupDictionary)
        json.dump(rootDictionary, outfile)    
if __name__ == '__main__':
    exit(main())